#include <sys/cdefs.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <signal.h>
#include "libc_private.h"

int __sys_sigaction(int sig, const struct sigaction * restrict act,
         struct sigaction * restrict oact)
{
	int rc;

	rc = syscall(SYS_sigaction, sig, act, oact);

	if (oact)
		__ccfi_addmac_global(&oact->sa_handler);

	return rc;
}

__weak_reference(__sys_sigaction, sigaction);
__weak_reference(__sys_sigaction, _sigaction);
